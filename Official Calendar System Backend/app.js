const express = require("express");
const createError = require("http-errors");
// const UserRouter = require("./routes");
const bodyParser = require("body-parser");
const app = express();
const UserRouter =require('./routes/routers') 
const TeacherCalendarRouter = require ('./routes/routers')
// const FigureRouter =require('./routes/routers') 
const cors = require ("cors")


app.use(cors())

app.use(bodyParser.json());
app.use(express.json());

app.use('/',UserRouter)
app.use('/',TeacherCalendarRouter)
// app.use('/',FigureRouter)

app.use((req, res) => {
    res.send(createError(404));
  });
  module.exports = app;