const Sequelize = require('sequelize');

const sequelize = new Sequelize({
    username: "root",
    hostname: "localhost",
    password: null,
    dialect: "mysql",
    database:"officialcalendar"
})

module.exports = sequelize;