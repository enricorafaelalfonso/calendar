const ProfileModel = require('../models/profile')

const createError = require("http-errors");

exports.updateProfile = async (req, res, next) => {
    const { id } = req.params;
    const { name, contact, email, birthdate, bio } = req.body;
  
    try {
      const user = await ProfileModel.findOne({ where: { id } });
      if (!user) {
        return res.status(404).json({ message: "User not found" });
      }
  
      // Update the user information
      if (name) {
        user.name = name;
      }
      if (contact) {
        user.contact = contact;
      }
      if (email) {
        user.email = email;
      }
      if (birthdate) {
        user.birthdate = birthdate;
      }
      if (bio) {
        user.bio = bio;
      }
      await user.save();
  
      // Create and sign a new JWT token with the updated user information
      const payload = {
        id: user.id,
        name: user.name,
        contact: user.contact,
        email: user.email,
        birthdate: user.birthdate,
        bio: user.bio,
      };
      const token = jwt.sign(payload, process.env.JWT_SECRET);
  
      res.json({ message: "User updated", token });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: "Server Error" });
    }
  };