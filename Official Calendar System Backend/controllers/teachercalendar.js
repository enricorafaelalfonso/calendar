const TeachercalendarModel = require ("../models/teachercalendar");

exports.getSchedule =  async (req, res) =>{
    try {
        const consults = await TeachercalendarModel.findAll();
        res.json(consults);
      } catch (err) {
        console.error(err);
        res.status(500).json({ error: "Unable to retrieve consultations." });
      }
    };
    
    exports.getSchedId = async (req, res) => {
      try {
        const consult = await TeachercalendarModel.findByPk(req.params.id);
        if (!consult) {
          res.status(404).json({ error: "Consultation not found." });
        } else {
          res.json(consult);
        }
      } catch (err) {
        console.error(err);
        res.status(500).json({ error: "Unable to retrieve consultation." });
      }
    };
    
    exports.createSchedule = async (req, res) => {
      try {
        const consult = await TeachercalendarModel.create(req.body);
        res.json(consult);
      } catch (err) {
        console.error(err);
        res.status(500).json({ error: "Unable to create schedule." });
      }
    };
    
    exports.deleteSchedule = async (req, res) => {
      try {
        const deletedRows = await TeachercalendarModel.destroy({
          where: { id: req.params.id },
        });
        if (deletedRows === 0) {
          res.status(404).json({ error: "Consultation not found." });
        } else {
          res.json({ success: true });
        }
      } catch (err) {
        console.error(err);
        res.status(500).json({ error: "Unable to delete consultation." });
      }
    };
