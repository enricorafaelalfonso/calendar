const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const passport = require ('passport');
const LocalStrategy = require ('passport-local');
const registerModel = require('../models/user'); // assuming we have a User model
const ErrorResponse = require('../utils/errorResponse');
const secret = 'your_secret_key_here';


exports.createUser = async (req, res) => {
  const { username, email, password } = req.body;

  try {
    // Check if user with email already exists
    const userExists = await registerModel.findOne({ where: { email } });

    if (userExists) {
      return res.status(409).json({ message: 'User already exists' });
    }

    // Hash password using bcrypt
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    // Create new user in database
    const newUser = await registerModel.create({
      username,
      email,
      password: hashedPassword
    });
    

    res.json({ message: 'User created successfully', user: newUser });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};



exports.loginUser = async (req, res) => {
  const { email, password } = req.body;
  const user = await registerModel.findOne({ where: { email } });
  
  try {
    console.log(user)
    if (!email || !password) {
      return res.status(400).send({
        success: false,
        message: "Please provide the necessary fields.",
      });
    }
    
    if (!user) {
      return res.status(400).send({
        success: false,
        message: "Invalid Credentials.",
      });
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      return res.status(400).send({
        success: false,
        message: "Invalid Credentials.",
      });
    }

    //Generated JWT token
    const token = jwt.sign({ id: user.id }, secret, { expiresIn: '1h' });

    res.status(200).send({
      success: true,
      message: "Login successful",
      data: { user, token },
    });
  } catch (error) {
    res.status(500).send({
      success: false,
      message: "Internal Server Error.",
      error: error.message,
    });
  }
};

exports.forgotPassword = async (req, res, next) => {
  const { email } = req.body;
  let user;

  try {
     user = await registerModel.findOne({ email });

    if (!user) {
      return next(new ErrorResponse('User not found with that email', 404));
    }

    const resetToken = user.getResetPasswordToken();

    await user.save();

    const resetUrl = `${req.protocol}://${req.get(
      'host'
    )}/api/v1/auth/resetpassword/${resetToken}`;

    const transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
      },
    });

    const message = {
      from: 'Your Company Name <noreply@yourcompany.com>',
      to: email,
      subject: 'Password Reset Request',
      html: `
        <p>You are receiving this email because you (or someone else) has requested the reset of the password for your account.</p>
        <p>Please click on the following link, or paste this into your browser to complete the process within 10 minutes of receiving it:</p>
        <p>${resetUrl}</p>
        <p>If you did not request this, please ignore this email and your password will remain unchanged.</p>
      `,
    };

    const info = await transporter.sendMail(message);

    console.log('Message sent: %s', info.messageId);

    res.status(200).json({ success: true, data: 'Email sent' });
  } catch (err) {
    console.error(err);
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;

    await user.save();

    return next(new ErrorResponse('Email could not be sent', 500));
  }
};


//RESET PASSWORD
exports.resetPassword = async (req, res, next) => {
  const resetPasswordToken = crypto
    .createHash("sha256")
    .update(req.params.resetToken)
    .digest("hex");

  try {
    const user = await registerModel.findOne({
      resetPasswordToken,
      resetPasswordExpire: { $gt: Date.now() },
    });

    if (!user) {
      return next(new ErrorResponse("Invalid Reset Token", 400));
    }

    user.password = req.body.password;
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;

    await user.save();

    res.status(201).json({
      success: true,
      data: "Password Reset Success",
    });
  } catch (error) {
    next(error);
  }
};

const sendToken = (user, statusCode, res) => {
  const token = user.getSignedToken();
  res.status(statusCode).json({ success: true, token });
};










