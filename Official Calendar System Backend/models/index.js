const sequelize = require("../config/db");
const UserModel = require("./user");
const ProfileModel = require("./profile");
const StudentcalendarModel = require ("./studentcalendar");
const TeachercalendarModel = require ("../models/teachercalendar")



UserModel.sync();
ProfileModel.sync();
StudentcalendarModel.sync();
TeachercalendarModel.sync();


module.exports = sequelize;