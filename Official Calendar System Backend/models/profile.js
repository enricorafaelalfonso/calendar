const Sequelize = require ('sequelize');
const sequelize = require ('../config/db');

const Profiles = sequelize.define("profile", {
    name:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    contact:{
        type: Sequelize.INTEGER,
    },
    email:{
        type: Sequelize.STRING,
    },
    birthdate: {
        type: Sequelize.INTEGER,
    },
    bio:{
        type: Sequelize.STRING,
    },
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
})

module.exports = Profiles;