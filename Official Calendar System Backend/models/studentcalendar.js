const Sequelize = require ('sequelize');
const sequelize = require ('../config/db');

const studentCalendar = sequelize.define("studentcalendar", {
    day:{
        type: Sequelize.STRING,
    },
    month:{
        type:Sequelize.STRING,
    },
    year:{
        type:Sequelize.STRING,
    },
    time:{
        type:Sequelize.STRING,
    },
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
})

module.exports = studentCalendar;