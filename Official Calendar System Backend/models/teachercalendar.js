const Sequelize = require ('sequelize');
const sequelize = require ('../config/db');

const teacherCalendar = sequelize.define("teachercalendar", {
    date:{
        type:Sequelize.STRING,
    },
    startTime:{
        type:Sequelize.STRING,
    },
    endTime:{
        type:Sequelize.STRING,
    },
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
})

module.exports = teacherCalendar;