const Sequelize = require('sequelize');
const sequelize = require("../config/db")

const Users = sequelize.define("users",{
    username: {
        type: Sequelize.STRING,
      },
      email: {
        type: Sequelize.STRING,
      },
      password: {
        type: Sequelize.STRING,
      },
      id:{
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement:true,
      },
    })
    

module.exports = Users;