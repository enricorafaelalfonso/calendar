const express = require("express");
const router = express.Router();
const UserController = require('../controllers/user')
const ProfileController = require ('../controllers/profile')
const TeacherController = require ('../controllers/teachercalendar')
// const FigureController = require('../controllers/figure')

//login
router.post('/register', UserController.createUser)
router.post('/login', UserController.loginUser)

// router.get('/', FigureController.getFigure)
// router.post('/', FigureController.createFigure)
// router.put('/:id', FigureController.updateFigure)
// router.get('/:id', FigureController.findFigure)
// router.delete('/:id', FigureController.deleteProduct)

//forgotPassword
router.post('/forgotPassword', UserController.forgotPassword);
router.put('/resetPassword/:resetToken', UserController.resetPassword);

//Calendar
router.get('/getSched', TeacherController.getSchedule);
router.get('/getSchedID/:id', TeacherController.getSchedId);
router.post('/createSched', TeacherController.createSchedule);
router.delete('/deleteSched/:id',TeacherController.deleteSchedule)

//Profile
router.put('/updateProfile/:id', ProfileController.updateProfile);

module.exports = router;