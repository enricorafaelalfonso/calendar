const http = require('http')
const app = require('./app')
const server = http.createServer(app)
const model = require('./models')

model.sync().then(() => {
    const port = process.env.PORT || 4000;
    server.listen(port,() =>
        console.log("Server in running at http://localhost:" + port)
    );

})